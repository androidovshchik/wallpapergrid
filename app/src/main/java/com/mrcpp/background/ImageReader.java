package com.mrcpp.background;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class ImageReader {
	
    public static String data[];
    public static Integer sum = 0;
    public static File dcim;
    public static List<File> imagelist;
    public static FilenameFilter filter;
    static BitmapFactory.Options options;
    
    public static void writeResources(Context context) throws IllegalAccessException, IOException {
    	String path = Environment.getExternalStorageDirectory().toString() + "/Background/";
    	File folder = new File(path), file;
    	folder.mkdirs();
    	OutputStream fOut = null;
    	
    	AssetManager assetManager = context.getAssets();
	    
    	for(int i = 0; i < LazyAdapter.data.length; i++) {
    		InputStream istr = assetManager.open(LazyAdapter.data[i]);
    		Bitmap bm = BitmapFactory.decodeStream(istr);
    		file = new File(path, Integer.toString(i) + ".jpg");
    		fOut = new FileOutputStream(file);
    	    bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
    	    fOut.flush();
    	    fOut.close();
    	}
    }
    
    public static void readFiles() {
		filter = new FilenameFilter() {
    	    public boolean accept(File dir, String name) {
    		    return ((name.endsWith(".jpg"))||(name.endsWith(".png")));
    	    }
        };
		
		File root = Environment.getExternalStorageDirectory();
        dcim = new File(root.getAbsolutePath());

        imagelist = new ArrayList<File>();

        File[] firstdir = dcim.listFiles();
        for(File file : firstdir) {
            if(file.isFile()) {
            	if(file.toString().endsWith(".jpg") || file.toString().endsWith(".png"))
            	    imagelist.add(file);
            } else if(file.isDirectory()) {
            	    listFilesInDir(file.listFiles());
            }
        }
        
        sum = imagelist.size();
        if(imagelist != null) {
            data = new String[imagelist.size()];
            for(int i = 0; i < imagelist.size(); i++) {
    		    data[i] = imagelist.get(i).getAbsolutePath();
    	    }
        }
    }
	
    private static void listFilesInDir(File[] path) {
    	for (File file : path) {
            if (file.isFile()) {
            	if(file.toString().endsWith(".jpg") || file.toString().endsWith(".png"))
            	    imagelist.add(file);
            }/* else if(file.isDirectory()) {
            	
            }*/
              else if(file == path[path.length - 1])
            	break;
        }
    }
    
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeBitmapFromFile(int position, int reqWidth, int reqHeight) {
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        
        BitmapFactory.decodeFile(LazyAdapter.data[position], options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(LazyAdapter.data[position], options);
    }
}