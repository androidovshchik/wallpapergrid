package com.mrcpp.background;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
 
public class MainActivity extends Activity {
	AlertDialog.Builder dialog;
	WallpaperManager myWallpaperManager;
	
	public static Integer picture;
	public static Boolean choosen = false;
	
	LazyAdapter adapter;
	GridView gridview;
	
    static Context context;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.grid);
	    
	    gridview = (GridView) findViewById(R.id.gridview);
	    adapter = new LazyAdapter(this);
	    gridview.setAdapter(adapter);
	    
	    context = getApplicationContext();
	    
	    new Thread(new Runnable() {
	        public void run() {
	        	try {
	    			ImageReader.writeResources(context);
	    		} catch (IOException e) {
	    			e.printStackTrace();
	    		}
				catch (IllegalAccessException e) {
					e.printStackTrace();
				}
	        }
	    }).start();
	    //ImageReader.readFiles();
        
	    dialog = new AlertDialog.Builder(MainActivity.this);
	    dialog.setMessage(getString(R.string.question));
	    dialog.setNegativeButton(getString(R.string.disagree), new OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
            	
            }
        });
	    dialog.setCancelable(true);
	    dialog.setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            	
            }
        });
	    gridview.setOnItemClickListener(new OnItemClickListener() {
	        public void onItemClick(AdapterView<?> parent, View v,
	                final int position, long id) {
	        	dialog.setPositiveButton(getString(R.string.agree), new OnClickListener() {
	                @Override
	    			public void onClick(DialogInterface dialoginter, int which) {
	                   myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());
	                   try {
	                	   myWallpaperManager.setBitmap(ImageAdapter.getBitmap(picture, context));
	                	   choosen = true;
	                	   adapter.notifyDataSetChanged();
	                	   gridview.setAdapter(adapter);
	                   } catch (IOException e) {
	                       e.printStackTrace();
	                   }
	    			}
	            });
	        	picture = position;
	        	dialog.show();
	        }
	    });
	}
	
	@Override
    protected void onDestroy() {
		gridview.setAdapter(null);
        super.onDestroy();
    }
}