package com.mrcpp.background;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LazyAdapter extends BaseAdapter {
    
    private Activity activity;
    static String[] data = {
        "id1.jpg", "id2.jpg", "id3.jpg", "id4.jpg", "id6.jpg", "id7.jpg", "id9.jpg"
    };
    private static LayoutInflater inflater=null;
    public ImageLoader imageLoader;
    View view;
	Point size;
    
    public LazyAdapter(Activity a) {
        activity = a;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader=new ImageLoader(activity.getApplicationContext());
    }

    public int getCount() {
        return data.length;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    
    @SuppressLint({ "InflateParams", "ViewHolder" })
	public View getView(int position, View convertView, ViewGroup parent) {
    	View view=convertView;
        view = inflater.inflate(R.layout.item, null);
        
        WindowManager wm = (WindowManager) MainActivity.context.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	size = new Point();
    	display.getSize(size);
    	int w = size.x;
    	
        RelativeLayout.LayoutParams params1 = new  RelativeLayout.LayoutParams((w-20)/2, (w-20)/2);
    	RelativeLayout.LayoutParams params2 = new  RelativeLayout.LayoutParams((w-20)/2, 25);
    	params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
    		
    	ImageView imageView = (ImageView) view.findViewById(R.id.grid_item_image);
    	imageView.setLayoutParams(params1);
    	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	imageView.setImageBitmap(ImageReader.decodeBitmapFromFile(position, (w-20)/2, (w-20)/2));
    		
    	TextView textView = (TextView) view .findViewById(R.id.grid_item_label);
    	if(MainActivity.choosen == true && MainActivity.picture == position){
    		textView.setBackgroundColor(0x80000000);
    		textView.setText("Установлено как обои");
    	}
    	textView.setLayoutParams(params2);
        
        imageLoader.DisplayImage(data[position], imageView);
        
        return view;
    }
}