package com.mrcpp.background;

import java.io.IOException;
import java.io.InputStream;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {
	
	static Context context;
	View view;
	Point size;
	
    public ImageAdapter(Context c) {
    	context = c;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

	@SuppressLint({ "InflateParams", "ViewHolder" })
	public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
    	size = new Point();
    	display.getSize(size);
    	int w = size.x;
    	
    	view = inflater.inflate(R.layout.item, null);
    	
        RelativeLayout.LayoutParams params1 = new  RelativeLayout.LayoutParams((w-20)/2, (w-20)/2);
    	RelativeLayout.LayoutParams params2 = new  RelativeLayout.LayoutParams((w-20)/2, 25);
    	params2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
    		
    	ImageView imageView = (ImageView) view.findViewById(R.id.grid_item_image);
    	imageView.setLayoutParams(params1);
    	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	imageView.setImageBitmap(ImageReader.decodeBitmapFromFile(position, (w-20)/2, (w-20)/2));
    		
    	TextView textView = (TextView) view .findViewById(R.id.grid_item_label);
    	if(MainActivity.choosen == true && MainActivity.picture == position){
    		textView.setBackgroundColor(0x80000000);
    		textView.setText("Установлено как обои");
    	}
    	textView.setLayoutParams(params2);
    	
        return view;
    }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	public static Bitmap getBitmap(int position, Context c) {
		AssetManager assetManager = c.getAssets();

	    InputStream istr;
	    Bitmap bitmap = null;
	    try {
	        istr = assetManager.open(LazyAdapter.data[position]);
	        bitmap = BitmapFactory.decodeStream(istr);
	    } catch (IOException e) {
	        // handle exception
	    }

	    return bitmap;
	}
}